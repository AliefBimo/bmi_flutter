class HistoryFields {
  static final String height = 'height';
  static final String weight = 'weight';
  static final String bmi = 'bmi';

  static List<String> getFields() =>[height, weight, bmi];
}