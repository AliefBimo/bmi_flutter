import 'package:gsheets/gsheets.dart';
import 'package:flutter_bmi_calculator/history/history.dart';



class HistorySheetsApi{
  static const _credentials = r'''
  {
  "type": "service_account",
  "project_id": "bmi-flutter-318503",
  "private_key_id": "8dad8c241da3045c7f4be0f7b775292f909bc7b7",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCQdxiDXGCUdTy6\ncZXZTsklTiEXqpmQnTxLJk4MmK+sOp8TxfeApZ7xcukwgD/LJF1Dt8awM9pIbSgk\nKoKnyQVqWPFIEjHsEi82NVeCNIAqRezaivCzDhZjL0yRQvX3t/ztbSd+SOXIqolD\nWW3ANznLz3D9WX8+I6tF8h/ENp4VCd3GhbuckdKj8Hk27Fw5J2sfc2Qe5pbhXfDW\nfiRI4kiLVMcgd0RIT36dtZpYnVveXWKowXu6VGMxfH6XL+dewa8iJMOihRKKx6LM\ndq1t8tfcrRVAItSAcTASO6dUKBeJj5nISNiVX5D7vJJ5SwV+dMS98ti/8+IeKZb/\nHGdMhquhAgMBAAECggEAASuc1MobnHJJc1l6mT3HmIQ2NYvP1g2HDLUPDHAzeqPI\nMIPh4B5/X9WCbP6Z/bRiBsubKgfmgqATQ10+jRtOXlzhegfvNHpWfHQAzSzNlcIW\no9PctLemT+gFANeD+H16qKYB7BaNOYOjrGpx3pz37HY2FDBcnPlTILRAJIHchJ86\naWVo9xWUAhF5JkCWARdVbNy1Eu7HghGQnWoNP9Ah75wKyCbaGpQrw/omwwHxaELQ\nz2Dkg9rSXfi07r3j7a3PvuBLgfXVfxKfpPBXgln9O/0hEpftbtbVKRB1ReAyPTLW\nsuxciOFKeBXOFORC+qamHvyuRFjXGAmfBR5drE0TsQKBgQDDBaeiutGatYLjRzGe\n5T3UY4g77WImwJJtjmHQx8wXN91FdwBJgjJzIqTiwgSA5sa4m4OV01RloGrKnitF\n4mXq8ClaGgnmloutRlCcBi4t7hEwVIK/uYxxuRblDEsqw22KlXtNq9r0gbrjDp3A\n/GU5YfUO3IBFs0FcUeLMzfAeMQKBgQC9oqrfqFJzd/HdjMWdgoUE56vnTgF3019E\nuw6YlFcDrL0FWbjpwCoy20RvTqfX8hY5IZsgwgrSmlhqrZ1GWmaXWo4L9dQP0ouX\ntm6otlT4iSYQwBwbCXw0QeHYsO4EPWbJjMINGeEEzgMr/e7+skafCE+DigS2D7v+\nK3TzORDYcQKBgBE2+ZIyvXfTl+cqDKZDBA8wbgEkNirHiuVi0R2V1V0jUEPmej8C\n4CMrprRhffZG6Lm4+IeMLQH/hA24ooc7sdVoN649aCxTk6bK8yiinwbs4dWLUDNO\nVKhhNzUmvafbslquUUEM73nvzahnTOORI/pJvFEAg84jQZy9mbCMaSqxAoGAaw8V\n1tTJT6UMqB5cRnM0M7OzKg+w5nwvkbblI6DDizCPOiEQMu8pI7MEbK6ENXbXswNU\n9+Z0g7ddaUSAwQIyXwphnUD4qsIbytfUm2zsha+wDM7YTy9IrpvvLA2fIkej6u+L\ncigFEFhaFBD1R1dQZWIkucdGp540bOcNzG6QIxECgYBBI1+XNiyPa/UAxRnd7xTr\nTxKyc2x5ssJbT7EoefmV9jxDFHJZhA72M6lgXa9HiECkyvO4fioabieBoSHZLSGQ\n0ro5LQycUYjR3ISwVzTTX+ku53wwq6PxXNfv16D16r7wv0RxHK22XJ4kAK7Rp/Z8\nyCU7c61LIwjRZFNnkKusYw==\n-----END PRIVATE KEY-----\n",
  "client_email": "bmi-flutter@bmi-flutter-318503.iam.gserviceaccount.com",
  "client_id": "112757496818261811123",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/bmi-flutter%40bmi-flutter-318503.iam.gserviceaccount.com"
}
''';
  static final _spreadsheetId = '13ovNk8GWyUE1Qz8FNhW45ywERWX6_U9nV3FjZ1uRzbs';
  static final _gsheets = GSheets(_credentials);
  static Worksheet _historySheet;

  static Future init() async {
    try{
      final spreadsheet = await _gsheets.spreadsheet(_spreadsheetId);
      _historySheet = await _getWorkSheet(spreadsheet, title: 'history');

      final firstRow = HistoryFields.getFields();
      _historySheet.values.insertRow(1, firstRow);
    }
    catch(e){
      print('Init Error: $e');
    }

  }

  static Future<Worksheet> _getWorkSheet(
      Spreadsheet spreadsheet, {String title,
      }) async {
    try {
      return await spreadsheet.addWorksheet(title);
    } catch (e) {
      return spreadsheet.worksheetByTitle(title);
    }
  }

  static Future insert(List<Map<String, dynamic>> rowList) async{
    if (_historySheet == null) return;
    _historySheet.values.map.appendRows(rowList);
  }
}