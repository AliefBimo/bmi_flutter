import 'package:flutter/material.dart';
import 'package:flutter_bmi_calculator/screens/calculator_screen.dart';
import 'package:flutter_bmi_calculator/history/history_sheets_api.dart';

Future main() async{
  WidgetsFlutterBinding.ensureInitialized();

  await HistorySheetsApi.init();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        primaryColor: Color(0xFF0A0E21),
        scaffoldBackgroundColor: Color(0xFF0A0E21),
      ),
      home: CalculatorScreen(),
    );

  }
}
